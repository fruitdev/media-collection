require 'rails_helper'

RSpec.describe User, :type => :model do
  it { should validate_presence_of :username }
  it { should validate_presence_of :email }
  it { should have_secure_password }

  it do
    should validate_length_of(:username).is_at_least(3).on(:create)
    should validate_length_of(:username).is_at_most(20)
  end

  it 'validate format of email' do
    expect(User.new(email: 'email')).to_not be_valid
  end
end