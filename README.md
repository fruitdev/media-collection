# README #

### How to run? ###

* git clone https://fruitdev@bitbucket.org/fruitdev/media-collection.git
* cd media-collection
* bundle install --without production
* rake db:migrate
* rails s
* Open [localhost:3000](http://localhost:3000)
* Live preview on heroku [fruitcollection.herokuapp.com/](http://fruitcollection.herokuapp.com/)
