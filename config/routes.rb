Rails.application.routes.draw do
  root 'collections#index'

  get 'collections/all' => 'pages#index'

  get 'pages/new'

  get 'pages/create'

  get 'pages/show'

  get 'pages/update'

  get 'pages/destroy'

  get 'signup' => 'users#new'

  get 'login' => 'sessions#new'

  post 'login' => 'sessions#create'

  delete 'logout' => 'sessions#destroy'

  resources :users, except: [:new]
  resources :collections, :images
end
