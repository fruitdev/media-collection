require 'rails_helper'

describe CollectionsController do
  describe 'guest user' do
    describe 'GET index' do
      before { get :index }

      it 'render :index template' do
        expect(response).to render_template(:index)
      end
    end

    describe 'GET show' do
      let(:collection) { FactoryGirl.create(:collection) }

      it 'renders :show template' do
        get :show, id: collection.id

        expect(response).to render_template(:show)
      end

      it 'assigns requested collection to @collection' do
        get :show, id: collection.id
        expect(assigns(:collection)).to eq(collection)
      end
    end
  end

  describe 'GET new' do
    before { get :new }

    it 'render error without signed user' do
      expect(response).to_not render_template(:new)
    end

    it 'error assigns new Collection to @collection without signed user' do
      expect(assigns(:collection)).to_not be_a_new(Collection)
    end
  end
end