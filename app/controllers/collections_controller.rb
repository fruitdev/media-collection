class CollectionsController < ApplicationController
  before_action :set_collection, only: [:edit, :update, :destroy, :show]
  before_action :require_user, except: [:index, :show]
  before_action :require_same_user, only: [:edit, :update, :destroy]

  def index
    @top_collections = Collection.last(6)
  end

  def new
    @collection = Collection.new
  end

  def create
    @collection = Collection.new(collection_params)
    @collection.user = current_user

    if @collection.save
      flash[:success] = "Collection created"
      redirect_to collection_path(@collection)
    else
      render 'new'
    end
  end

  def show
    @collection = Collection.find(params[:id])

    @collection_images = @collection.images.paginate(page: params[:page], per_page: 3)
  end

  def edit

  end

  def update
    if @collection.update(collection_params)
      flash[:success] = 'Collection Updated!'
      redirect_to collection_path
    else
      render 'edit'
    end
  end

  def destroy
    @collection = Collection.find(params[:id])

    @collection.destroy

    redirect_to collections_path
  end

  private
    def collection_params
      params.require(:collection).permit(:title, :description, :image)
    end

    def set_collection
      @collection = Collection.find(params[:id])
    end

    def require_same_user
      if current_user != @collection.user and !current_user.admin?
        flash[:danger] = 'You can only edit or delete your own collections'
        redirect_to root_path
      end
    end
end
