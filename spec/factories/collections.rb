FactoryGirl.define do
  factory :collection do
    sequence(:title) { |n| "Collection #{n}" }
    description 'My Description'
    user_id 1

    factory :public_collection do
      privacy :public_access
    end

    factory :private_collection do
      privacy :private_access
    end
  end
end