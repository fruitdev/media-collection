require 'rails_helper'

RSpec.describe Image, :type => :model do
  it { should validate_presence_of :link }
  it { should validate_presence_of :collection_id }
  it { should validate_presence_of :picture }

  it do
    should validate_length_of(:link).is_at_least(5).on(:create)
    should validate_length_of(:link).is_at_most(50)
  end

  it 'validate link format' do
    expect(Image.new(link: 'ya.ru')).to_not be_valid
  end
end