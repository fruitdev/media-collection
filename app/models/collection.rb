class Collection < ActiveRecord::Base
  belongs_to :user
  has_many :images

  validates :user_id, :title, :description, presence: true
  validates :title, length: { minimum: 5, maximum: 50 }
  validates :description, length: { minimum: 10 }
end
