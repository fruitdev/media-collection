class RemoveColums < ActiveRecord::Migration
  def change
    remove_column :collections, :image
    remove_column :collections, :link
    remove_column :collections, :image_file_name
    remove_column :collections, :image_content_type
    remove_column :collections, :image_file_size
    remove_column :collections, :image_updated_at
  end
end
