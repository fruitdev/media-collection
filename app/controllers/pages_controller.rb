class PagesController < ApplicationController
  def index
    @collections = Collection.paginate(page: params[:page], per_page: 9)
  end
end
