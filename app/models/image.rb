class Image < ActiveRecord::Base
  belongs_to :collection
  belongs_to :user

  VALID_URL = /https?:\/\/[\S]+/

  validates :link, presence: true, length: { minimum: 5 }, format: { with: VALID_URL }
  validates :collection_id, :picture, presence: true

  has_attached_file :picture, styles: { medium: "300x300>" }
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
end
