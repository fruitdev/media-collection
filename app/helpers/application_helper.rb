module ApplicationHelper
  def gravatar_for(user, options = { size: 40 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = 'https://secure.gravatar.com/avatar/' + gravatar_id.to_s + '?s=' + size.to_s
    image_tag(gravatar_url, alt: user.username, class: 'img-responsive')
  end

  def user_info
    user = User.find(session[:user_id])
  end
end
