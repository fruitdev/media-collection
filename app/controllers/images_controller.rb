class ImagesController < ApplicationController
  before_action :set_image, only: [:edit, :update, :destroy]
  before_action :require_same_user, only: [:edit, :update, :destroy]

  def new
    if current_user.collections.count < 1
      flash[:danger] = 'You must create collection before.'
      redirect_to root_path
    else
      @image = Image.new
    end
  end

  def create
    @image = Image.new(image_params)

    @image.user = current_user

    if @image.save
      flash[:success] = 'Photo attached!'
      redirect_to collection_path(image_params[:collection_id])
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @image.update(image_params)
      flash[:success] = 'Image updated!'
      redirect_to collection_path(image_params[:collection_id])
    else
      render 'edit'
    end
  end

  def destroy
    @image.destroy

    redirect_to collection_path(@image.collection)
  end

  private
    def image_params
      params.require(:image).permit(:link, :picture, :collection_id)
    end

    def set_image
      @image = Image.find(params[:id])
    end

    def require_same_user
      if current_user != @image.user and !current_user.admin?
        flash[:danger] = 'You can only edit or delete your own images'
        redirect_to root_path
      end
    end
end
