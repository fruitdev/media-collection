require 'rails_helper'

RSpec.describe Collection, :type => :model do
  it { should validate_presence_of :title }
  it { should validate_presence_of :description }
  it { should validate_presence_of :user_id }

  it do
    should validate_length_of(:title).is_at_least(5).on(:create)
    should validate_length_of(:title).is_at_most(50)
    should validate_length_of(:description).is_at_least(10).on(:create)
  end
end