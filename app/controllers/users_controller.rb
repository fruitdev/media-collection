class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update, :show]
  before_action :require_user, only: [:edit]
  before_action :require_admin, only: [:destroy]
  before_action :require_same_user, only: [:edit, :update, :destroy]

  def index
    @users = User.paginate(page: params[:page], per_page: 9)
  end

  def new
    redirect_to root_path if logged_in?
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      session[:user_id] = @user.id
      flash[:success] = "Welcome to \"Media Collection\", #{@user.username}!"
      redirect_to user_path(@user)
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    if @user.update(user_params)
      flash[:success] = 'Profile is updated!'
      redirect_to edit_user_path(@user)
    else
      render 'edit'
    end
  end

  def show
    @user_collections = @user.collections.paginate(page: params[:page], per_page: 3)
  end

  def destroy
    @user = User.find(params[:id])

    @user.destroy
    flash[:danger] = 'Account and all collections by user deleted!'
    redirect_to users_path
  end

  private
    def user_params
      params.require(:user).permit(:username, :email, :password)
    end

    def set_user
      @user = User.find(params[:id])
    end

    def require_same_user
      if current_user != @user and !current_user.admin?
        flash[:danger] = 'Access denied!'
        redirect_to root_path
      end
    end

    def require_admin
      if logged_in? and !current_user.admin?
        flash[:danger] = 'Only admin user can perform that action'
        redirect_to root_path
      end
    end
end
