class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :link
      t.integer :user_id
      t.integer :collection_id
    end
  end
end
